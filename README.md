Servisleri başlatmak için proje ana dizininde;


```bash
cd deploy/docker
sudo docker-compose up -d 
```


Servisleri durdurmak için yine proje ana dizininde;

```bash
cd deploy/docker
sudo docker-compose stop
```

